{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators #-}

module Bikeshed.Api
  ( api
  , API
  , Project(..) -- TODO: Remove this
  , Task(..) -- TODO: Remove this
  ) where

import Data.Aeson (ToJSON, FromJSON)
import GHC.Generics (Generic)
import Servant

data Project = Project
  { id :: Int
  , name :: Text
  } deriving (ToJSON, FromJSON, Generic)

data Task = Task
  { id :: Int
  , name :: Text
  , projectId :: Int
  , completed :: Bool
  } deriving (ToJSON, FromJSON, Generic)

type API =
  "api" :> "tasks" :> Get '[JSON] [Task] :<|>
  "api" :> "tasks" :> Capture "taskid" Int :> Get '[JSON] Task :<|>
  "api" :> "tasks" :> Capture "taskid" Int :> ReqBody '[JSON] Task :> PostNoContent '[JSON] NoContent :<|>
  "api" :> "projects" :> Get '[JSON] [Project] :<|>
  "api" :> "projects" :> Capture "projectid" Int :> Get '[JSON] Project :<|>
  "api" :> "projects" :> Capture "projectid" Int :> ReqBody '[JSON] Project :> PostNoContent '[JSON] NoContent :<|>
  Raw

api :: Proxy API
api = Proxy
