module Bikeshed.Server (start, start') where

import qualified Network.Wai as Wai
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Logger (withStdoutLogger)
import Servant (Application)

start :: Warp.Port -> Application -> IO ()
start port app =
  withStdoutLogger $ \f -> do
    let settings = Warp.defaultSettings & Warp.setLogger f & Warp.setPort port
    Warp.runSettings settings app

log :: Wai.Middleware
log app req res = do
  let method = Wai.requestMethod req
  let path = Wai.rawPathInfo req
  -- body <- Wai.requestBody req
  -- putStrLn $ method <> " " <> path <> " " <> body
  putStrLn $ method <> " " <> path
  app req res

start' :: Warp.Port -> Application -> IO ()
start' port app = run port (log app)
