{-# LANGUAGE DuplicateRecordFields #-}

module Main where

import Network.Wai.Application.Static (defaultWebAppSettings, ssIndices, ssRedirectToIndex)
import Servant
import System.Environment (getArgs)
import WaiAppStatic.Types (unsafeToPiece)

import Bikeshed.Api (api, API, Project(..), Task(..)) -- TODO: Don't import Project and Task
import Bikeshed.Server (start, start')

data DB = DB
  { tasks :: [Task]
  , projects :: [Project]
  }

fakeDb :: DB
fakeDb = DB
  { tasks =
      [ Task 1 "Do the dishes" 1 True
      , Task 2 "Walk the dog" 1 False
      , Task 3 "Book flight to Hawaii" 2 False
      ]
  , projects =
      [ Project 1 "Chores"
      , Project 2 "Vacation prep"
      ]
  }

getTasksHandler :: DB -> Handler [Task]
getTasksHandler (DB ts _) = return ts

getTaskHandler :: DB -> Int -> Handler Task
getTaskHandler (DB ts _) u = do
  let maybeTask :: Maybe Task
      maybeTask = safeHead . filter ((== u) . (id :: Task -> Int)) $ ts
  case maybeTask of
    Just x  -> return x
    Nothing -> throwError
      $ err404 { errBody = "Could not find task with ID: " <> show u }

postTaskHandler :: Int -> Task -> Handler NoContent
postTaskHandler id task = return NoContent

getProjectsHandler :: DB -> Handler [Project]
getProjectsHandler (DB _ ps) = return ps

getProjectHandler :: DB -> Int -> Handler Project
getProjectHandler (DB _ ps) u = do
  let maybeProject :: Maybe Project
      maybeProject = safeHead . filter ((== u) . (id :: Project -> Int)) $ ps
  case maybeProject of
    Just x  -> return x
    Nothing -> throwError
      $ err404 { errBody = "Could not find project with ID: " <> show u }

postProjectHandler :: Int -> Project -> Handler NoContent
postProjectHandler id project = return NoContent

server :: DB -> Server API
server db =
  getTasksHandler db
    :<|> getTaskHandler db
    :<|> postTaskHandler
    :<|> getProjectsHandler db
    :<|> getProjectHandler db
    :<|> postProjectHandler
    :<|> serveDirectoryWith settings
  where settings =
          (defaultWebAppSettings "static/")
            { ssIndices = [ unsafeToPiece "index.html" ] }

app :: DB -> Application
app db = serve api (server db)

main :: IO ()
main = do
  args <- getArgs
  let port = safeHead args >>= readMaybe & fromMaybe 8080
  putStrLn $ "bikeshed-server running at http://localhost:" ++ show port
  start' port (app fakeDb)
