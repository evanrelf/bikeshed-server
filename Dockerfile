FROM fpco/stack-build:lts-12.14 as build
WORKDIR /bikeshed-server
ADD . .
RUN stack build --system-ghc --no-nix --copy-bins --local-bin-path dist/

FROM fpco/haskell-scratch:integer-gmp
COPY --from=build /bikeshed-server/dist/bikeshed-server /usr/local/bin/bikeshed-server
ENTRYPOINT ["/usr/local/bin/bikeshed-server"]
